<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1> Buat Account Baru </h1>
    <h2> Sign Up Form </h2>
    <form action="/welcome" method="POST">
        @csrf
        <label> First Name : </label> <br>
        <input type="text" name="namadepan"> <br><br>
        <label> Last Name : </label> <br>
        <input type="text" name="namabelakang"> <br><br>
        <div class="options">
            <label> Gender : </label> <br>
            <label class="radio"> 
                <input name="jenis_kelamin" type="radio" value="L" checked="checked">Laki Laki</option>
            </label> <br>
            <label class="radio"> 
                <input name="jenis_kelamin" type="radio" value="P">Perempuan</option>
            </label> <br>
            <label class="radio"> 
                <input name="jenis_kelamin" type="radio" value="0"> Other</option>
            </label> <br><br>
        </div>
        <label name="nationality">  Nationality : </label>
        <select name="area">
            <option value="Indonesia" selected="selected">Indonesian</option>
            <option value="Singapore"> Singapore </option>
            <option value="Malaysia"> Malaysia </option>
            <option value="Thailand"> Thailand </option>
        </select> <br><br>
        <div class="options">
            <label> Language Spoke : </label> <br>
            <label class="checkbox"> 
                <input name="bahasa" type="checkbox" value="indonesia" checked="checked">Bahasa Indonesia</option>
            </label> <br>
            <label class="checkbox"> 
                <input name="bahasa" type="checkbox" value="english">English</option>
            </label> <br>
            <label class="checkbox">
                <input name="bahasa" type="checkbox" value="arabic">Arabic</option>
            </label> <br>
            <label class="checkbox">
                <input name="bahasa" type="checkbox" value="japanese">Japanese</option>
            </label> <br><br>
        </div> 
        <label> Bio : </label> <br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br><br>
        <input type="submit" value="Kirim">
    </form>
</body>
</html>